FROM docker.elastic.co/logstash/logstash:7.14.0

RUN bin/logstash-plugin install logstash-output-gelf logstash-patterns-core
